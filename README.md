# 基于selenium的12306抢票程序

#### 项目介绍
自动根据条件查询余票信息，当有票的时候，自动预定，并发送QQ邮件通知订票人。但是需要自己输入账号登录，并在弹窗中再次手动输入车票区间等信息。如果没有票，则会持续查询180次，之后结束。程序默认的是普通硬座。

#### 软件架构
软件架构说明


#### 安装教程

1. 需要安装`Selenium`库。

#### 使用说明

1. 默认抢票的是普通火车，且座位为硬座。
2. 如果想要高铁座位。可以自行修改代码，将96、99、135行的信息改为需要的座位信息即可。
3. 为了保证抢票成功，请确保所抢车次有硬座。除非你修改车次相关的代码。

#### 参与贡献

1. 暂时没有。


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)